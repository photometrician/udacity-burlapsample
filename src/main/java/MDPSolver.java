import burlap.behavior.singleagent.planning.stochastic.valueiteration.ValueIteration;
import burlap.behavior.statehashing.DiscreteStateHashFactory;
import burlap.domain.singleagent.graphdefined.GraphDefinedDomain;
import burlap.oomdp.auxiliary.DomainGenerator;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.*;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.core.State;

public class MDPSolver {
    DomainGenerator dg;
    Domain domain;
    State initState;
    State state1;
    RewardFunction rf;
    TerminalFunction tf;
    DiscreteStateHashFactory hashFactory;

    public MDPSolver(int numStates, int numActions, double[][][] probabilitiesOfTransitions,
                     double[][][] rewards) {
        this.dg = new GraphDefinedDomain(numStates);

        // transition probabilities
        for (int i = 0; i < numStates; i ++) {
            for (int j = 0; j < numActions; j ++) {
                for (int k = 0; k < numStates; k ++) {
                    ((GraphDefinedDomain) this.dg).setTransition(i, j, k, probabilitiesOfTransitions[i][j][k]);
                }
            }
        }

        this.domain = this.dg.generateDomain();
        this.initState = GraphDefinedDomain.getState(this.domain, 0);
        this.rf = new MDPSolverRewardFunction(rewards);
        this.tf = new NullTermination();
        this.hashFactory = new DiscreteStateHashFactory();
    }

    public static class MDPSolverRewardFunction implements RewardFunction {
        double[][][] rewards;

        public MDPSolverRewardFunction(double[][][] rewards) {
            this.rewards = rewards;
        }

        @Override
        public double reward(State s, GroundedAction a, State sprime) {
            int sidSrc = GraphDefinedDomain.getNodeId(s);
            int sidDst = GraphDefinedDomain.getNodeId(sprime);
            int action = Integer.valueOf(a.action.getName().substring("action".length()));
            return rewards[sidSrc][action][sidDst];
        }
    }

    private ValueIteration computeValue(double gamma) {
        double maxDelta = 0.0001;
        int maxIterations = 1000;
        ValueIteration vi = new ValueIteration(this.domain, this.rf, this.tf, gamma,
                this.hashFactory, maxDelta, maxIterations);
        vi.planFromState(this.initState);
        return vi;
    }

    public double valueOfState(int state, double gamma) {

        ValueIteration vi = computeValue(gamma);
        for (State s : vi.getAllStates()) {
            int stateId = GraphDefinedDomain.getNodeId(s);
            if (stateId == state){
                return vi.value(s);
            }
        }
        throw new IllegalArgumentException("invalid state. state=" + state);
    }
}
