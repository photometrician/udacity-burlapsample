// BURLAP documentation: http://burlap.cs.brown.edu/doc/

import burlap.behavior.singleagent.Policy;
import burlap.behavior.singleagent.planning.commonpolicies.GreedyQPolicy;
import burlap.behavior.singleagent.planning.stochastic.valueiteration.ValueIteration;
import burlap.behavior.statehashing.DiscreteStateHashFactory;
import burlap.domain.singleagent.graphdefined.GraphDefinedDomain;
import burlap.domain.singleagent.graphdefined.GraphTF;
import burlap.oomdp.auxiliary.DomainGenerator;
import burlap.oomdp.core.*;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.core.State;


public class SecondMDP {
    DomainGenerator dg;
    Domain domain;
    State initState;
    State state1;
    RewardFunction rf;
    TerminalFunction tf;
    DiscreteStateHashFactory hashFactory;

    public SecondMDP(double p1, double p2) {
        int numStates = 6;
        this.dg = new GraphDefinedDomain(numStates);

        //actions from initial state 0
        ((GraphDefinedDomain) this.dg).setTransition(0, 0, 0, p1);
        ((GraphDefinedDomain) this.dg).setTransition(0, 0, 1, 1.0 - p1);
        ((GraphDefinedDomain) this.dg).setTransition(0, 1, 2, 1.);
        //actions from initial state 1
        ((GraphDefinedDomain) this.dg).setTransition(1, 2, 3, 1.0 - p2);
        ((GraphDefinedDomain) this.dg).setTransition(1, 2, 5, p1);
        ((GraphDefinedDomain) this.dg).setTransition(1, 3, 4, 1.);
        //actions from initial state 2
        ((GraphDefinedDomain) this.dg).setTransition(2, 0, 1, 1.0);
        //actions from initial state 3
        ((GraphDefinedDomain) this.dg).setTransition(3, 0, 1, 1.0);
        //actions from initial state 4
        ((GraphDefinedDomain) this.dg).setTransition(4, 0, 5, 1.0);


        this.domain = this.dg.generateDomain();
        this.initState = GraphDefinedDomain.getState(this.domain, 0);
        this.state1 = GraphDefinedDomain.getState(this.domain, 1);
        this.rf = new SecondMDPRewardFunction();
        this.tf = new GraphTF(5);
        this.hashFactory = new DiscreteStateHashFactory();
    }

    public static class SecondMDPRewardFunction implements RewardFunction {

        public SecondMDPRewardFunction() {
        }

        @Override
        public double reward(State s, GroundedAction a, State sprime) {
            int sidSrc = GraphDefinedDomain.getNodeId(s);
            int sidDst = GraphDefinedDomain.getNodeId(sprime);
            double r = 0;

            if (sidSrc == 0 && sidDst == 0) {
                r = -1;
            } else if (sidSrc == 0 && sidDst == 1) {
                r = 3;
            } else if (sidSrc == 0 && sidDst == 2) {
                r = 1;
            } else if (sidSrc == 1 && sidDst == 3) {
                r = 1;
            } else if (sidSrc == 1 && sidDst == 4) {
                r = 2;
            } else if (sidSrc == 1 && sidDst == 5) {
                r = 0;
            } else if (sidSrc == 2 && sidDst == 1) {
                r = 0;
            } else if (sidSrc == 3 && sidDst == 1) {
                r = 0;
            } else if (sidSrc == 4 && sidDst == 5) {
                r = 0;
            } else {
                throw new RuntimeException("invalid transition. srcstate:" + sidSrc + " deststate:" + sidDst);
            }

            return r;
        }
    }

    private ValueIteration computeValue(double gamma) {
        double maxDelta = 0.0001;
        int maxIterations = 1000;
        ValueIteration vi = new ValueIteration(this.domain, this.rf, this.tf, gamma,
                this.hashFactory, maxDelta, maxIterations);
        return vi;
    }

    private ValueIteration computeState0Value(double gamma) {
        ValueIteration vi = computeValue(gamma);
        vi.planFromState(this.initState);
        return vi;
    }


    public String bestActions(double gamma) {
        // Return one of the following Strings
        // "a,c"
        // "a,d"
        // "b,c" 
        // "b,d"
        // based on the optimal actions at states S0 and S1. If 
        // there is a tie, break it in favor of the letter earlier in
        // the alphabet (so if "a,c" and "a,d" would both be optimal, 
        // return "a,c").

        ValueIteration vi = computeState0Value(gamma);
        Policy p = new GreedyQPolicy(vi);
        State s0 = GraphDefinedDomain.getState(domain, 0);
        State s1 = GraphDefinedDomain.getState(domain, 1);
        String s0Action = p.getAction(s0).actionName()
                .replaceAll("action0", "a").replaceAll("action1", "b");
        String s1Action = p.getAction(s1).actionName()
                .replaceAll("action2", "c").replaceAll("action3", "d");
        return s0Action + "," + s1Action;
    }

    public static void main(String[] args) {
        double p1 = 0.5;
        double p2 = 0.5;
        SecondMDP mdp = new SecondMDP(p1, p2);

        double gamma = 0.5;
        System.out.println("Best actions: " + mdp.bestActions(gamma));
    }
}